/**
Filename: registers.c
Author: Tessa Herzberger and Kiley LeClair
Class: enel487
Project: lab2
*/

/**
This file contains the registers used in this program, as well as the setupRegs function.
*/

#include "main.h"
#include "registers.h"

//LED Registers
volatile uint32_t * regRCC_APB2ENR;
volatile uint32_t * regGPIOB_ODR;
volatile uint32_t * regGPIOB_CRH;
volatile uint32_t * regGPIOB_BSRR;
volatile uint32_t * regGPIOB_BRR;


//Serial Registers
volatile uint32_t * regGPIOA_CRL;
volatile uint32_t * regGPIOA_ODR;
volatile uint32_t * regGPIOA_CRH;
volatile uint32_t * regGPIOA_BSRR;
volatile uint32_t * regGPIOA_BRR;

//USART Registers
volatile uint32_t * regRCC_APB1ENR;
volatile uint32_t * regUSART_CR1;
volatile uint32_t * regUSART_CR2;
volatile uint32_t * regUSART_BRR;
volatile uint32_t * regUSART_DR;
volatile uint32_t * regUSART_SR;


//The setupRegs function initializes all of the registers.
void setupRegs(void)
{
	regRCC_APB2ENR = (volatile uint32_t *)RCC_APB2ENR;
  regGPIOB_ODR =  (volatile uint32_t *)GPIOB_ODR ; 
	regGPIOB_CRH =  (volatile uint32_t *)GPIOB_CRH ; 
	regGPIOB_BSRR =  (volatile uint32_t *)GPIOB_BSRR ; 
  regGPIOB_BRR =  (volatile uint32_t *)GPIOB_BRR ; 
	
	regGPIOA_CRL = (volatile uint32_t *)GPIOA_CRL;
  regGPIOA_ODR =  (volatile uint32_t *)GPIOA_ODR ; 
	regGPIOA_CRH =  (volatile uint32_t *)GPIOA_CRH ; 
	regGPIOA_BSRR =  (volatile uint32_t *)GPIOA_BSRR ; 
  regGPIOA_BRR =  (volatile uint32_t *)GPIOA_BRR ; 
 
	
	regRCC_APB1ENR = (volatile uint32_t *)RCC_APB1ENR;
	regUSART_CR1 = (volatile uint32_t *)USART_CR1;
	regUSART_CR2 = (volatile uint32_t *)USART_CR2;
	regUSART_BRR = (volatile uint32_t *)USART_BRR;
	regUSART_DR = (volatile uint32_t *)USART_DR;
	regUSART_SR = (volatile uint32_t *)USART_SR;
}
