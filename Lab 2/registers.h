/**
Filename: registers.h
Author: Tessa Herzberger and Kiley LeClair
Class: enel487
Project: lab2
*/

/**
This file contains all of the addresses for the registers, as well as the setupRegs function prototype.
*/

#ifndef REGISTERS_H
#define REGISTERS_H


#define PERIPH_BASE           ((uint32_t)0x40000000)
#define AHBPERIPH_BASE        (PERIPH_BASE + 0x20000)
#define RCC_BASE              (AHBPERIPH_BASE + 0x1000)
#define RCC_APB2ENR           (RCC_BASE + 0x18)
#define APB2PERIPH_BASE       (PERIPH_BASE + 0x10000)
#define GPIOB_BASE            (APB2PERIPH_BASE + 0x0C00)
#define GPIOB_ODR             (GPIOB_BASE + 0x0C)
#define GPIOB_CRH             (GPIOB_BASE + 0x04)
#define GPIOB_BSRR            (GPIOB_BASE  + 0x10)
#define GPIOB_BRR             (GPIOB_BASE  + 0x14)
#define INITIAL_LIGHTS        ((uint32_t)0x00000100)
#define SETUP_LIGHT_LOOP      ((uint32_t)0x01000200)
#define CYLON_RIGHT           ((uint32_t)0x80004000)

//Serial Registers
#define GPIOA_BASE       ((uint32_t)0x40010800)
#define GPIOA_CRL     (GPIOA_BASE)
#define GPIOA_ODR        (GPIOA_BASE + 0x0C)
#define GPIOA_CRH   (GPIOA_BASE + 0x04)
#define GPIOA_BSRR   	   (GPIOA_BASE  + 0x10)
#define GPIOA_BRR       (GPIOA_BASE  + 0x14)

//USART Registers
#define RCC_APB1ENR		 (RCC_BASE + 0x1C)
#define USART_BASE    (0x40004400)
#define USART_CR1     (USART_BASE +0x0C) //USART Enable
#define USART_CR2     	   (USART_BASE +0x10) //USART Clock Enable
#define USART_BRR       (USART_BASE  + 0x08)
#define USART_DR      (USART_BASE + 0x04)
#define USART_SR     	   (USART_BASE)

void setupRegs(void);

#endif
