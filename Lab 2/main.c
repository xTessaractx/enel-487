/**
Filename: main.c
Author: Tessa Herzberger and Kiley LeClair
Class: enel487
Project: lab2
*/

#include <stdio.h>
#include "main.h"
#include "serial_driver_interface.h"
#include <ctype.h>

/**
This is the main function. It initializes the values of many variables, then determines what input the user selected.
Finally, it resets everything.
*/
int main()
{
	//Initialize the Constant values that will be used for the Help Statement, as well as the console string.
	const char CONSOLE[30] = "ENEL487_IS_AWESOME > ";
	const char HELP_1[60] = "- To turn on an LED, enter the following command,\0";
	const char HELP_2[16] ="\n\r    LED # ON\0";
	const char HELP_3[60] = "- To turn off an LED, enter the following command,\0";
	const char HELP_4[17] ="\n\r    LED # OFF\0";
	const char HELP_5[70] = " - To check the status of an LED, enter the following command:\0";
	const char HELP_6[16] ="\n\r    STATUS #\0";
	const char HELP_7[60] = " - To turn on all LEDs, enter the following command:\0";
	const char HELP_8[14] ="\n\r    ALL ON\0";	
	const char HELP_9[60] = " - To turn all off LEDs, enter the following command:\0";
	const char HELP_10[15] ="\n\r    ALL OFF\0";
	const char HELP_11[60] = " - To display the time, enter the following command:\0";
	const char HELP_12[12] ="\n\r    TIME\0";
	const char HELP_13[60] = " - To display the date, enter the following command:\0";
	const char HELP_14[12] ="\n\r    DATE\0";
	const char HELP_15[60] = "\n\r  where # is the number of the LED you want to turn on.\0";
	
	//Initialize the loop counter, i and the data_array
	int i = 0;
	char data_array[50];
	
	//Call the setupRegs function and serial_open function.
	setupRegs();
	serial_open();

	//Setup the the LED's on the board
  * regRCC_APB2ENR |= 0x08; // Enable Port B clock
	* regGPIOB_ODR  &= ~0x0000FF00;          /* switch off LEDs                    */
	* regGPIOB_CRH  = 0x33333333; //50MHz  General Purpose output push-pull
	
	//Initialize the data_array values to Null.
	for (i = 0; i < 50; i++)
		data_array[i] = '\0';
			
	while(1)
	{
		//Print the console.
		print_string(CONSOLE);
		
		//Ensure that I is set to zero.
		i = 0;
		
		for (i = 0; i < 50; i++)
		{
			//Get the user input.
			data_array[i] = getbyte();
			
			//Check if the input was a backspace and correct for the backspace.
			if(data_array[i] == 8 || data_array[i] == 127)
			{	
				data_array[i] = '\0';
				sendbyte('\b');
					if (i != 0)
						i--;
			}
		
			//Convert all of the values of data_array to uppercase.
			data_array[i] = TO_UPPER (data_array[i]);				
			
			/**
					The Following if statements determine what statement was entered by the user.
					The appropriate functions are called according to the user's entry.
																																											**/
			//If LED # ON was entered
			if (data_array[0] == 'L' && data_array[1] == 'E' && data_array[2] == 'D' && data_array[3] == ' '
				&& data_array[5] == ' ' && data_array[6] == 'O' && data_array[7] == 'N' && data_array[8] == 0xD)
			{
				on(data_array[4]);
				i = Good_To_Go(i);
			}

			//If LED # OFF was entered
			else if (data_array[0] == 'L' && data_array[1] == 'E' && data_array[2] == 'D' && data_array[3] == ' '
				&& data_array[5] == ' ' && data_array[6] == 'O' && data_array[7] == 'F' && data_array[8] == 'F'
				&& data_array[9] == 0xD)
			{
				off(data_array[4]);
				i = Good_To_Go(i);
			}
			
			//If ALL ON was entered
			else if (data_array[0] == 'A' && data_array[1] == 'L' && data_array[2] == 'L' && data_array[3] == ' '
				&& data_array[4] == 'O' && data_array[5] == 'N' && data_array[6] == 0xD)
			{
				* regGPIOB_BSRR |= 0x0000FF00;
				i = Good_To_Go(i);
			}
			
			//If ALL OFF was entered
			else if (data_array[0] == 'A' && data_array[1] == 'L' && data_array[2] == 'L' && data_array[3] == ' '
				&& data_array[4] == 'O' && data_array[5] == 'F' && data_array[6] == 'F' && data_array[7] == 0xD)
			{
				* regGPIOB_BSRR |= 0xFF000000;
				Good_To_Go(i);
			}
			
			//If STATUS # was entered
			else if (data_array[0] == 'S' && data_array[1] == 'T' && data_array[2] == 'A' && data_array[3] == 'T'
				&& data_array[4] == 'U' && data_array[5] == 'S' && data_array[6] == ' ' && data_array[8] == 0xD)
			{
					get_status(data_array[7]);
							i = Good_To_Go(i);
			}
			
			//If DATE was entered
			else if (data_array[0] == 'D' && data_array[1] == 'A' && data_array[2] == 'T' && data_array[3] == 'E'
				&& data_array[4] == 0xD)
			{
				print_date();			
				i = Good_To_Go(i);
			}
			
			//If TIME was entered
			else if (data_array[0] == 'T' && data_array[1] == 'I' && data_array[2] == 'M' && data_array[3] == 'E'
				&& data_array[4] == 0xD)
			{
				print_time();		
				i = Good_To_Go(i);
			}
			
			//If HELP was entered
			else if (data_array[0] == 'H' && data_array[1] == 'E' && data_array[2] == 'L' && data_array[3] == 'P'
				&& data_array[4] == 0xD)
			{
				print_string("\n\r");
				print_string(HELP_1);
				print_string(HELP_2);
				print_string("\n\r");
				print_string(HELP_3);
				print_string(HELP_4);
				print_string("\n\r");
				print_string(HELP_5);
				print_string(HELP_6);
				print_string("\n\r");
				print_string(HELP_7);
				print_string(HELP_8);
				print_string("\n\r");
				print_string(HELP_9);
				print_string(HELP_10);
				print_string("\n\r");
				print_string(HELP_11);
				print_string(HELP_12);
				print_string("\n\r");
				print_string(HELP_13);
				print_string(HELP_14);
				print_string("\n\r");
				print_string(HELP_15);
				print_string("\n\r");
			}
			
			//Clear the value in the GPIOA_CRL register	
			* regGPIOA_CRL = 0x44444B44;
			
			//Send the byte to the console.
			sendbyte(data_array[i]);
		}
			
		restart(data_array);
	}
}

/**
The print_string function takes a const char array,
then prints the array. It returns nothing.
*/
void print_string(const char output[])
{
	int i = 0;
	
	while (output[i] != '\0')
	{
		sendbyte(output[i]);
		i++;
	}
	
	return;
}

/**
The TO_UPPER function takes a char data
and converts it to uppercase.
*/
char TO_UPPER (char data)
{
		if (data >=TO_UPPER_LOWER_BOUNDARY && data <= TO_UPPER_UPPER_BOUNDARY)
			data = data - 32;
		
		return data;
}

/**
The Good_To_Go function takes an integer, i,
which is the loop counter from the main function,
prints a new line, and sets i to 100.
It returns i.
*/
int Good_To_Go(int i)
{
			sendbyte('\n');
			sendbyte('\r');
			i = 100;
	
	return i;
}

/**
The get_status function takes an integer,
then determines which light the user
would like to view the status of using a case statement.
It returns nothing.
*/
void get_status(int data)
{
	sendbyte('\n');
	sendbyte('\r');
	
	switch(data)
	{
		case '1':
			if ((* regGPIOB_ODR & 0x00000100) != 0x0)
				sendbyte('1');
			else
				sendbyte('0');
			break;
		case '2':
			if ((* regGPIOB_ODR & 0x00000200) != 0x0)
				sendbyte('1');
			else
				sendbyte('0');
			break;
		case '3':
			if ((* regGPIOB_ODR & 0x00000400) != 0x0)
				sendbyte('1');
			else if ((* regGPIOB_ODR & 0x00000400) == 0x0)
				sendbyte('0');
			break;
		case '4':
			if ((* regGPIOB_ODR & 0x00000800) != 0x0)
				sendbyte('1');
			else
				sendbyte('0');
			break;
		case '5':
			if ((* regGPIOB_ODR & 0x00001000) != 0x0)
				sendbyte('1');
			else
				sendbyte('0');
			break;
		case '6':
			if ((* regGPIOB_ODR & 0x00002000) != 0x0)
				sendbyte('1');
			else
				sendbyte('0');
			break;
		case '7':
			if ((* regGPIOB_ODR & 0x00004000) != 0x0)
				sendbyte('1');
			else
				sendbyte('0');
			break;
		case '8':
			if ((* regGPIOB_ODR & 0x00008000) != 0x0)
				sendbyte('1');
			else
				sendbyte('0');
			break;
		default:
			* regGPIOB_BSRR = 0x0;
			break;
	}	

	return;
}

/**
The on function takes an integer,
then determines which light the user
would like to turn on using a case statement.
It returns nothing.
*/
void on(int data)
{
	switch(data)
	{
		case '1':
			* regGPIOB_BSRR |= INITIAL_LIGHTS;
			break;
		case '2':
			* regGPIOB_BSRR |= INITIAL_LIGHTS << 1;
			break;
		case '3':
			* regGPIOB_BSRR |= INITIAL_LIGHTS << 2;
			break;
		case '4':
			* regGPIOB_BSRR |= INITIAL_LIGHTS << 3;
			break;
		case '5':
			* regGPIOB_BSRR |= INITIAL_LIGHTS << 4;
			break;
		case '6':
			* regGPIOB_BSRR |= INITIAL_LIGHTS << 5;
			break;
		case '7':
			* regGPIOB_BSRR |= INITIAL_LIGHTS << 6;
			break;
		case '8':
			* regGPIOB_BSRR |= INITIAL_LIGHTS << 7;
			break;
		default:
			* regGPIOB_BSRR = 0x0;
			break;
	}
	
	return;
}

/**
The off function takes an integer,
then determines which light the user
would like to turn off using a case statement.
It returns nothing.
*/
void off(int data)
{
	switch(data)
	{
		case '1':
			* regGPIOB_BSRR |= (uint32_t) RESET_LIGHTS;
			break;
		case '2':
			* regGPIOB_BSRR |= (uint32_t) RESET_LIGHTS << 1;
			break;
		case '3':
			* regGPIOB_BSRR |= (uint32_t) RESET_LIGHTS << 2;
			break;
		case '4':
			* regGPIOB_BSRR |= (uint32_t) RESET_LIGHTS << 3;
			break;
		case '5':
			* regGPIOB_BSRR |= (uint32_t) RESET_LIGHTS << 4;
			break;
		case '6':
			* regGPIOB_BSRR |= (uint32_t) RESET_LIGHTS << 5;
			break;
		case '7':
			* regGPIOB_BSRR |= (uint32_t) RESET_LIGHTS << 6;
			break;
		case '8':
			* regGPIOB_BSRR |= (uint32_t) RESET_LIGHTS << 7;
			break;
	default:
			* regGPIOB_BSRR = 0x0;
			break;
	}
	
	return;
}

/**
The print_time function prints the time by calling the sendbyte function.
It returns nothing.
*/
void print_time()
{
	//Declare a loop counting variable.
	int i = 0;
	
	//Print a new line
	sendbyte('\n');
	sendbyte('\r');
	sendbyte(' ');
	
	//Print the time
	for (i = 0; i < 11; i++)
		sendbyte(__TIME__[i]);
	
	return;
}

/**
The print_date function prints the date by calling the sendbyte function.
It returns nothing.
*/
void print_date()
{
	//Declare a loop counting variable.
	int i = 0;
	
	//Print a new line
	sendbyte('\n');
	sendbyte('\r');
	sendbyte(' ');
	
	//Print the date
	for (i = 0; i < 11; i++)
		sendbyte(__DATE__[i]);
	
	return;
}


/**
The Restart function takes a character array and resets
all of its values to a null character.
It also resets the GPIOA_CRL register.
It returns nothing.
*/
void restart(char data_array[50])
{
	//Declare a loop counting variable.
	int i = 0;
	
	//Reset the data_array.
	for (i = 0; i < 50; i++)
		data_array[i] = '\0';
	
	//Reset the GPIOA_CRL register
		* regGPIOA_CRL = 0x44444B44;
	
	return;
}
