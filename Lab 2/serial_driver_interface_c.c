/**
Filename: serial_driver_interface_c.c
Author: Tessa Herzberger and Kiley LeClair
Class: enel487
Project: lab2
*/

#include "serial_driver_interface.h"
#include "main.h"

/** Configure and enable the device. */
void serial_open(void)
{
	*regRCC_APB2ENR |= 0x0000000D; //Enable GPIOA Clock, GPIOB Clock and AFIO Clocks.
	
	*regGPIOA_CRL	|= 	0x00000B80;	//Enable Tx (Alt. Output push/pull, 50MHz; 1011) and Rx (floating input; 1000)of USART2
	
	*regRCC_APB1ENR |= 0x00020000; //Enable USART2 Clock
	*regRCC_APB1ENR |= 0x00040000;

	*regUSART_CR1 |=  0x0000200C; //Enable USART Transmission and Reception.
	
	//Configure Baud Rate
	*regUSART_BRR |= 36000000/9600;
	*regUSART_CR1 &= 0xFFFFEBFF; //set m bit and pce to zero
	*regUSART_CR2 &= 0xFFFFCFFF; //set stop bit to zero
}

/**
Send an 8-bit byte to the serial port, using the configured
bit-rate, # of bits, etc. Returns 0 on success and non-zero on
failure.
@param b the 8-bit quantity to be sent.
@pre must have already called serial_open()
*/
void sendbyte(char data) //Transmission
{
	uint32_t USART_SR_TXE = 0x00000080;
	//Check if TXE is high
	while ((*regUSART_SR & USART_SR_TXE) == 0)
	{
	}
	
	* regUSART_DR = data;

	return;
}

/**
Gets an 8-bit character from the serial port, and returns it.
@pre must have already called serial_open()
*/
char getbyte(void) //Reception
{		
	uint32_t USART_SR_RNXE = 0x00000020;
	//Check if RNXE is high
	while ((*regUSART_SR & USART_SR_RNXE) == 0)
	{
	}
	
	return * regUSART_DR;
}
