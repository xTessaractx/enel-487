/**
Filename: main.h
Author: Tessa Herzberger and Kiley LeClair
Class: enel487
Project: lab2
*/
/**This file contains all of the register pointers,
static constant integers, and function prototypes used within the main.c file. */

#include "registers.h"
#include <stdint.h>



//Registers are defined in register.h and register.c
extern volatile uint32_t * regRCC_APB2ENR;	 
extern volatile uint32_t * regGPIOB_ODR;
extern volatile uint32_t * regGPIOB_CRH;
extern volatile uint32_t * regGPIOB_BSRR;
extern volatile uint32_t * regGPIOB_BRR;
  
//Serial
extern volatile uint32_t * regGPIOA_CRL; //DONE
extern volatile uint32_t * regGPIOA_ODR; //DONE
extern volatile uint32_t * regGPIOA_CRH; //DONE
extern volatile uint32_t * regGPIOA_BSRR; //DONE
extern volatile uint32_t * regGPIOA_BRR; //DONE

//USART Stuff
extern volatile uint32_t * regRCC_APB1ENR;
extern volatile uint32_t * regUSART_CR1;
extern volatile uint32_t * regUSART_CR2;
extern volatile uint32_t * regUSART_BRR;
extern volatile uint32_t * regUSART_DR;
extern volatile uint32_t * regUSART_SR;
	
static const char * LED = "LED";
static const char * ON = "ON";



static const int TO_UPPER_LOWER_BOUNDARY = 97;
static const int TO_UPPER_UPPER_BOUNDARY = 122;
static volatile uint32_t RESET_LIGHTS = 0x01000000;


void print_string(const char [100]);
char TO_UPPER (char);
int Good_To_Go(int);
void get_status(int);
void on(int);
void off(int);
void print_time(void);
void print_date(void);
void restart(char data_array[50]);
