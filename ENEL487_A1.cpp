/**
		Name: Tessa Herzberger
		SID: 200342876
		
		Institution: University of Regina
		Class: ENEL 479
		Professor: Karim Naqvi

		Assignment: #1
		Due: 19 September 2017

*/

#define NULL 0

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <limits.h>

using namespace std;

/**
	The Complex struct contains 4 feilds.
	The str feild contains the operation selection and numbers that the user inputs.
	The operation feild contains the operation selection that the user entered in str.
	The equation_value array contains the numbers that the user entered in str.
	The solution array contains the soluton from the mathematic operation that the user entered in str.
*/
struct Complex
{
	string str;
	char operation;

	float equation_value[4];
	float solution[2];
};


Complex divide(Complex entry);
Complex multiply(Complex entry);
Complex add(Complex entry);
Complex subtract(Complex entry);


/**
	This is the main function. It asks for the input string in the form of "a 1 2 3 4",
	then obtains the first letter of the string and determines what mathematic operation
	will be performed (addition, subtraction, multiplication or division).
	After that, it displays the the output.
	This process repeats until 'q' is entered to exit the while loop and the program.
*/
int main()
{
	//Variable declarations
	Complex entry;
	int inc = 0;

	//This while loop continues until 'q' or 'Q' is entered.
	while (entry.operation != 'q' || entry.operation != 'Q')
	{
		cerr << "Enter exp: " << endl;
	
		//Declare a temporary character array, and set all fields to NULL.
		char temp[100];

		for(int i = 0; i < 100; i++)
			temp[i] = NULL;

		//Use the fgets function inputs the values from the temp character array into the entry.str string.
		if (fgets(temp, 100, stdin) != NULL)
		{
			for(int i = 0; i < 100; i++)
			{
				if (temp[i] != NULL)
					entry.str += temp[i];
			}
		}
			
		//Takes the first character in the entry.str string and inputs it into the operation field of entry.
		entry.operation = entry.str[0];
		//Erases the first character of the entry.str string.
		entry.str = entry.str.erase(0, 1);

		//Take the rest of the entry.str string and inputs it into the equation_value array fields of entry.
		istringstream equation_stream(entry.str);

		equation_stream >> entry.equation_value[0] >> entry.equation_value[1] >> entry.equation_value[2] >> entry.equation_value[3];

		//Call the add function.
		if (entry.operation == 'a' || entry.operation == 'A')
		{
			entry = add(entry);
		}

		//Call the subtract function.
		else if (entry.operation == 's' || entry.operation == 'S')
		{
			entry = subtract(entry);
		}

		//Call the multiply function.
		else if (entry.operation == 'm' || entry.operation == 'M')
		{
			entry = multiply(entry);
		}

		//Call the divide function.
		else if (entry.operation == 'd' || entry.operation == 'D')
		{
			entry = divide(entry);
		}

		//Exit the program and the loop
		else if (entry.operation == 'q' || entry.operation == 'Q')
			return 0;
		
		//Output an error message if an invalid value was entered.
		else
			cerr << "You have entered an invalid value." << endl << endl;
		
		//Output the solution.
		cout << entry.solution[0] << " + j" << entry.solution[1] << endl;

		//Clear the string field.
		entry.str = "";
	}

	return 0;	
}


/** 
The add function takes a Complex variable,
adds the real and imaginary parts of the equation,
then returns the Complex variable to the main function.
*/
Complex add(Complex entry)
{
	entry.solution[0] = entry.equation_value[0] + entry.equation_value[2];
	entry.solution[1] = entry.equation_value[1] + entry.equation_value[3];

	return entry;
}

/** 
The multiply function takes a Complex variable,
multiplies the real and imaginary parts of the equation,
then returns the Complex variable to the main function.
*/
Complex multiply(Complex entry)
{
	entry.solution[0] = (entry.equation_value[0] * entry.equation_value[2]) - (entry.equation_value[1] * entry.equation_value[3]);
	entry.solution[1] = (entry.equation_value[0] * entry.equation_value[3]) + (entry.equation_value[1] * entry.equation_value[2]);

	return entry;
}

/** 
The subtract function takes a Complex variable,
subtracts the real and imaginary parts of the equation,
then returns the Complex variable to the main function.
*/
Complex subtract(Complex entry)
{
	entry.solution[0] = entry.equation_value[0] - entry.equation_value[2];
	entry.solution[1] = entry.equation_value[1] - entry.equation_value[3];

	return entry;
}

/** 
The divide function takes a Complex variable,
multiplies the numerator and denominator by the conjugate of the numerator,
then divides the real and imaginary parts of the equation.
After that, it returns the Complex variable to the main function.
*/
Complex divide(Complex entry)
{
	Complex temp_complex;

	temp_complex.solution[0] = (entry.equation_value[0] * entry.equation_value[2]) + (entry.equation_value[1] * entry.equation_value[3]);
	temp_complex.solution[1] = (entry.equation_value[1] * entry.equation_value[2]) - (entry.equation_value[0] * entry.equation_value[3]);

	entry.solution[0] = temp_complex.solution[0] / ((entry.equation_value[2] * entry.equation_value[2]) + (entry.equation_value[3] * entry.equation_value[3]));
	entry.solution[1] = temp_complex.solution[1] / ((entry.equation_value[2] * entry.equation_value[2]) + (entry.equation_value[3] * entry.equation_value[3]));

	return entry;
}