Program: ENEL487_A1.cpp
Name: Tessa Herzberger
SID: 200342876
Institution: University of Regina
Class: ENEL 479
Professor: Karim Naqvi
Assignment: #1
Due: 19 September 2017


GENERAL USAGE NOTES:
- This program takes an input string in the form of "char float float float float"
  and calculates a arithmetic operation with two real numbers and two
  imaginary numbers.
- The arithmetic operation is determined by the char.
	- 'a' or 'A' for addition.
	- 's' or 'S' for subtraction.
	- 'm' or 'M' for multiply.
	- 'd' or 'D' for division.
	- 'q' or 'Q' to quit the program.
- The first and third float numbers are the real values.
- The second and fourth float numbers are the imaginary values.

STRUCT
- A struct named "Complex" was created. It contains a four fields: a string,
  a char, a float array of size four, and a second float array of size two.
- The string "str" is used to hold the input from a file or user.
- The operation field contains the operation selection that the user entered in str.
- The equation_value array contains the numbers that the user entered in str.
- The solution array contains the soluton from the mathematic operation that the user entered in str.

FUNCTIONS:
- main
	- The main function asks for the input string in the form of "a 1 2 3 4",
	  then obtains the first letter of the string and determines what mathematic operation
	  will be performed (addition, subtraction, multiplication or division).
	  After that, it displays the the output.
	  This process repeats until 'q' is entered to exit the while loop and the program.
- add
	- The add function takes a Complex variable, adds the real and
	  imaginary components, then saves the values to the Complex's
	  solution field. It then returns the Complex variable to the main function.
- subtract
	- The subtract function takes a Complex variable, subtracts the real and
	  imaginary components, then saves the values to the Complex's
	  solution field. It then returns the Complex variable to the main function.
- multiply
	- The add function takes a Complex variable, adds the real and
	  imaginary components, then saves the values to the Complex's
	  solution field. It then returns the Complex variable to the main function.
- divide
	- The add function takes a Complex variable, adds the real and
	  imaginary components, then saves the values to the Complex's
	  solution field. It then returns the Complex variable to the main function.

COMPILING AND RUNNING
- In order to compile and run this program in command line, use the following lines:
	  g++ -o A1 ENEL487_A1.cpp
	  ./A1
- To compile and run this program in batch mode in command line, use the following lines:
  (NOTE: input.txt and output.txt will be changed to the desired input and output files.
  The input file needs to be added to the same directory as the .cpp file)
	  g++ -o A1 ENEL487_A1.cpp
	  ./A1 <input.txt >output.txt